Rails.application.routes.draw do
devise_for :users

  root to: "home#index"

  resources :users, only: [ :show, :edit, :update ] do 
    get :search, on: :collection   
  end

  resources :projects
  resources :investitors, only: :index
  resources :shortlists, only: [:index, :destroy] do
    post 'add_investitor/:investitor_id', action: :create, as: :add_investitor, on: :collection
  end



  get "testing_views/:folder/:view_to_load" => "testing_views#index"
#  get "users/update_states", as: "update_states"


  match "/states/:country_id" => "states#index", via: "get"
  match "/cities/:state_id" =>  "cities#index", via: "get"
end
