class ProjectParticipantsForm {

  constructor(button_to_add, submit_button){
    this.button_to_add = button_to_add;
    this.submit_button = submit_button;

    this.submit_button.prop("disabled", true);

    let url = this.button_to_add.data("url-investitors");
    let obj = this;

    $.getJSON(url, {}, function(data){
      obj.investitors = data;
      obj.submit_button.prop("disabled", false);
    });
  }


  add_new_element() {
    let position = $(".participant_fields").length;
    let fields = this._get_hidden_field(`project[participants_attributes][${position}][id]`);
    fields += this._get_hidden_field(`project[participants_attributes][${position}][_destroy]`, "false");
    fields += this._get_investitor_fields(position, this._get_investitor_options());
    fields += this._get_amount_invested_fields(position);
    fields += this._get_rating_fields(position);
    fields += this._get_delete_button();

    $("#participants").append(`<div class="participant_fields">${fields}</div>`);
  }


  _get_delete_button() {
    return `<button class='delete_parcitipant' type='button'>Delete</button>`;
  }


  _get_investitor_fields(position, fields) {
    return `<div class="input select required project_participant_investitor">
              <label class="decimal required" for="project_participant_investitor_id">
                <abbr title="required">*</abbr>
                Investitor
              </label>
              <select class="select required" name="project[participants_attributes][${position}][investitor_id]">
                ${fields}
              </select>
            </div>`;
  }


  _get_amount_invested_fields(position) {
    return `<div class="input decimal required project_participant_amount_invested">
              <label class="decimal required" for="project_participant_amount_invested">
                <abbr title="required">*</abbr>
                Amount invested
              </label>
              <input class="numeric decimal required" name="project[participants_attributes][${position}][amount_invested]" />
            </div>`;
  }


  _get_rating_fields(position) {
    return `<div class="input integer required project_participant_rating">
              <label class="integer required" for="project_participant_rating">
                <abbr title="required">*</abbr>
                Rating
              </label>
              <input class="numeric integer required" name="project[participants_attributes][${position}][rating]" type="number" />
            </div>`;
  }


  _get_hidden_field(name, value = ""){
    return `<input name="${name}" value="${value}" type="hidden" />`;
  }


  _get_investitor_options() {
    let html = `<option value=""></option>`;

    for(let i = 0; i < this.investitors.length; i++) {
      html += `<option value="${this.investitors[i].id}">${this.investitors[i].name}</option>`;
    }

    return html;
  }

}
