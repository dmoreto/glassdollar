$(document).ready(function(){

  let add_button = $("#add_participant");
  let submit_button = $("#submit_project");

  let participant_form = new ProjectParticipantsForm(add_button, submit_button);

  $("#add_participant").click(function(){
    participant_form.add_new_element();
  });

  $("div#participants").on("click", "div.participant_fields button.delete_parcitipant", function(){
    console.log("teste");
    let parent_element = $(this).closest(".participant_fields");
    parent_element.find("input.destroy_field").val("true");
    parent_element.hide();
  });
});
