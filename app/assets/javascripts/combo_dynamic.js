$(document).ready(function() {
    $('#countries').change(function() {
     
         var country_id = $(this).val();
         var servidor = '/states/'+country_id;
   
         $.getJSON( servidor, function(data){
  
          var state = [];
          var options = "<option value=''>select...</option>";
  
            $.each(data, function(key, val){
              options +='<option value="'+val.id+'">'+val.name+'</option>';
          
            });  
            
            $("#states").html(options);        
  
             get_city();
         
         }); 
      });
   });


    function get_city(){
      $('#states').change(function(){
         var state_id = $(this).val();    
         var servidor = '/cities/'+state_id;
     
      $.getJSON( servidor, function(data){
            var state = [];
            var options = "<option value=''>select...</option>";
  
              $.each(data, function(key, val){
              options +='<option value="'+val.id+'">'+val.name+'</option>';
          
            });  
  
            $("#cities").html(options);
       
      });
     });
    }
 

