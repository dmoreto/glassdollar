class ProjectPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.where(founder_id: user.id)
    end
  end


  def index?
    permitted_access?
  end


  def new?
    permitted_access?
  end


  def create?
    new?
  end


  def show?
    permitted_access?
  end


  def edit?
    permitted_access?
  end


  def update?
    edit?
  end


  def destroy?
    permitted_access?
  end


  private


  def permitted_access?
    user.profile.founder?
  end
end
