class ShortlistPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.where(founder_id: user.id)
    end
  end


  def index?
    permitted_access?
  end


  def create?
    permitted_access?
  end


  def destroy?
    permitted_access?
  end


  private


  def permitted_access?
    user.profile.founder?
  end
end
