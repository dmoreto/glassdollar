module ShortlistsHelper
  def add_to_shortlist_button(investitor)
    unless current_user.shortlists.where(investitor: investitor).any?
      link_to "Add to Shortlist", add_investitor_shortlists_path(investitor), method: :post
    end
  end
end
