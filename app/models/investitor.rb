class Investitor < User
  default_scope -> { with_profile("investitor") }

  has_many :shortlists
  has_many :participations, class_name: "Participant"
end
