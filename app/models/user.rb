class User < ApplicationRecord
  extend Enumerize

  has_one :address
  accepts_nested_attributes_for :address


  # possible values: :confirmable, :lockable, :timeoutable, :recoverable, :trackable, :validatable and :omniauthable
  devise :database_authenticatable, :registerable, :rememberable

  has_many :shortlists, foreign_key: "founder_id"
  has_many :projects, foreign_key: "founder_id"

  serialize :profile, Array
  enumerize :profile, in: [ :investitor, :founder ], multiple: true

  scope :with_profile, ->(profile) { where("profile ~* ?", profile) }

end
