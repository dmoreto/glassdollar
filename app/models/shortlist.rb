class Shortlist < ApplicationRecord
  belongs_to :investitor, class_name: "User"
  belongs_to :founder, class_name: "User"

  validates :investitor_id, uniqueness: { scoped_to: :founder_id } 
end
