class Participant < ApplicationRecord
  belongs_to :investitor, class_name: "User"
  belongs_to :project, optional: true

  validates :amount_invested, presence: true
  validates :rating, presence: true
end
