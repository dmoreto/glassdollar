class Project < ApplicationRecord
  belongs_to :founder, class_name: "User"

  has_many :participants, dependent: :destroy, autosave: true
  accepts_nested_attributes_for :participants, allow_destroy: true

  validates :name, presence: true
  validates :description, presence: true
end
