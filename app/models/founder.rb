class Founder < User
  default_scope -> { with_profile("founder") }

  has_many :shortlists
end
