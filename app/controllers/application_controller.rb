class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include Pundit

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  respond_to :html, :json

  before_action :authenticate_user!

  def configure_devise_permitted_params
     devise_parameter_sanitizer.permit(:sign_up) do |user_params|
       user_params.permit(:email,
                          :name,
                          :profile,
                          :home_page,
                          :avatar
                         )
     end
   end


   def user_not_authorized
     flash[:error] = "You are not authorized to perform this action"
     redirect_to root_path
   end
end
