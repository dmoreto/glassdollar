class TestingViewsController < ActionController::Base

  layout "application"

  def index
    render "#{params[:folder]}/#{params[:view_to_load]}"
  end
end
