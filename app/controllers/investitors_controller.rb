class InvestitorsController < ApplicationController

   def index
     authorize Investitor
     @investitor = Investitor.all

      respond_to do |format|
         format.html # index.html.erb
         format.json { render json: @investitor }
       end
    end
end
