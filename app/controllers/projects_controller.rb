class ProjectsController < ApplicationController
  before_action :load_projects, only: :index
  before_action :load_project, only: [:show, :edit, :update, :destroy]
  before_action :authorize_user


  def index
  end


  def new
    build_project
    @project.participants.build
  end


  def create
    build_project
    save_project
  end


  def show
  end


  def edit
  end


  def update
    build_project
    save_project
  end


  def destroy
    destroy_project
  end


  private


  def destroy_project
    @project.destroy
    respond_with @project
  end


  def save_project
    respond_with @project if @project.save
  end


  def load_project
    @project = project_scope.find(params[:id])
  end


  def load_projects
    @projects = project_scope
  end


  def build_project
    @project ||= project_scope.build
    @project.attributes = project_params
  end


  def project_params
    return {} unless params[:project].present?
    params.require(:project).permit(:id, :name, :description,
                                    participants_attributes: [ :id, :_destroy, :investitor_id, :amount_invested, :rating ])
  end


  def project_scope
    policy_scope(Project).all
  end


  def authorize_user
    authorize Project
  end

end
