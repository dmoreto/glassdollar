class UsersController < ApplicationController

  before_action :load_user
  

  def index
  end

  def show 
  end
  

  def update
    build_user
    save_user
  end

  def search
    @search = User.with_profile("investitor")
    respond_with @search
  end

  private


  def save_user
    respond_with @user if @user.save
  end


  def build_user
    @user ||= user_scope.build
    @user.attributes = user_params
  end


  def user_params
    return {} unless params[:user].present?
    params.require(:user).permit(:email, :name, :description, :home_page,profile: [],
                                 address_attributes: [:street,:number,:district,:additional_information,:city_id] 
                                )
  end


  def load_user
    @user = user_scope.find(params[:id])
  end


  def user_scope
    User.all
  end

end
