class ShortlistsController < ApplicationController
  before_action :load_shortlists, only: :index
  before_action :load_shortlist, only: :destroy
  before_action :authorize_user


  def index
  end


  def create
    build_shortlist
    save_shortlist
  end


  def destroy
    destroy_shortlist
  end


  private


  def destroy_shortlist
    @shortlist.destroy
    respond_with @shortlist
  end


  def save_shortlist
    respond_with(@shortlist, location: investitors_path ) if @shortlist.save!
  end


  def load_shortlist
    @shortlist = shortlist_scope.find(params[:id])
  end


  def load_shortlists
    @shortlists = shortlist_scope
  end


  def build_shortlist
    @shortlist = shortlist_scope.build
    @shortlist.investitor_id = params[:investitor_id]
  end


  def shortlist_scope
    policy_scope(Shortlist).all
  end


  def authorize_user
    authorize Shortlist
  end

end
