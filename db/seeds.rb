# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

   country = Country.create([
                 {:name => 'Brasil', :abbreviation =>'BR'},
                 {:name => 'África do Sul', :abbreviation => 'ZA'}, 
                 {:name => 'Alemanha', :abbreviation => 'DE'},
                 {:name => 'Itália', :abbreviation =>'IT'},
                 {:name => 'Macedónia', :abbreviation =>'MK'}
                 ])


  state = State.create([
                        {:name => 'Ribeiro Gonçalves', :country_id => country[0].id },
                        {:name => 'Uruçuí', :country_id => country[0].id},
                        {:name => 'Teresina', :country_id => country[0].id },
                        {:name => 'Satira', :country_id => country[1].id },
                        {:name => 'Ribeiro Gonçalves', :country_id => country[1].id },
                        {:name => 'Uruca', :country_id => country[1].id},
                        {:name => 'Teresinopoles', :country_id => country[2].id },
                        {:name => 'Teresinha', :country_id => country[2].id },
                        {:name => 'Danielvis', :country_id => country[2].id },
                        {:name => 'Evertones', :country_id => country[3].id},
                        {:name => 'Henri', :country_id => country[3].id },
                        {:name => 'VaiVai', :country_id => country[3].id },
                        {:name => 'Cariacica', :country_id => country[4].id },
                        {:name => 'Arace', :country_id =>  country[4].id},
                        {:name => 'Angra', :country_id =>  country[4].id }
                        ])


   City.create([
      {:name =>'Charikar',:state_id => state[0].id},
      {:name =>'Herat',:state_id => state[0].id}, 
      {:name =>'Durban',:state_id => state[1].id}, 
      {:name =>'Porto Isabel',:state_id => state[1].id},
      {:name =>'Luanda',:state_id => state[2].id}, 
      {:name =>'Lobito',:state_id => state[2].id},  
      {:name =>'Amstetten',:state_id => state[3].id}, 
      {:name =>'Ansfelden',:state_id => state[3].id},  
      {:name =>'Mosteiros',:state_id => state[4].id}, 
      {:name =>'Pedra Badejo',:state_id => state[4].id},  
      {:name =>'Espargos',:state_id => state[5].id}, 
      {:name =>'Ribeira Brava',:state_id => state[5].id},
      {:name =>'Calheta de São Miguel',:state_id => state[6].id}, 
      {:name =>'Cova Figueira',:state_id => state[6].id},
      {:name =>'Picos',:state_id => state[7].id}, 
      {:name =>'Assens',:state_id => state[7].id},
      {:name =>'Beder',:state_id => state[8].id}, 
      {:name =>'Bogense',:state_id => state[8].id}, 
      {:name =>'Brande',:state_id => state[9].id},
      {:name =>'Farum',:state_id => state[9].id}, 
      {:name =>'Fredericia',:state_id => state[10].id},  
      {:name =>'Hadsten',:state_id => state[10].id},
      {:name =>'Herning',:state_id => state[11].id},  
      {:name =>'Horsens',:state_id => state[11].id}, 
      {:name =>'Califórnia',:state_id => state[13].id},
      {:name =>'Arizona',:state_id => state[13].id},   
      {:name =>'Virgínia',:state_id => state[14].id}, 
      {:name =>'Alberta',:state_id => state[14].id}  
      ])
