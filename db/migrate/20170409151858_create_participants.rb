class CreateParticipants < ActiveRecord::Migration[5.0]
  def change
    create_table :participants do |t|
      t.references :investitor, foreign_key: { to_table: :users }
      t.references :project
      t.decimal :amount_invested, precision: 11, scale: 2
      t.integer :rating

      t.timestamps
    end
  end
end
