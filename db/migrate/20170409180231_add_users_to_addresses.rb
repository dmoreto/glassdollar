class AddUsersToAddresses < ActiveRecord::Migration[5.0]
  def change
     add_foreign_key :addresses, :users, foreign_key: true        
  end
end
