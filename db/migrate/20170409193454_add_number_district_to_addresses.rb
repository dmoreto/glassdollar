class AddNumberDistrictToAddresses < ActiveRecord::Migration[5.0]
  def change
    add_column :addresses, :number, :integer
    add_column :addresses, :district, :string
  end
end
