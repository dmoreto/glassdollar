class CreateAddresses < ActiveRecord::Migration[5.0]
  def change
    create_table :addresses do |t|
      t.string :street
      t.string :additional_information
      t.references :city, foreign_key: true
      t.timestamps
    end
  end
end
