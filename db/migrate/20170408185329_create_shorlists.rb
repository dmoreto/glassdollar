class CreateShorlists < ActiveRecord::Migration[5.0]
  def change
    create_table :shortlists do |t|
      t.references :investitor, foreign_key: { to_table: :users }
      t.references :founder, foreign_key: { to_table: :users }
      t.timestamps
    end
  end
end
